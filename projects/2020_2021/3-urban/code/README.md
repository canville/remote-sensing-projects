# Mapping the Grenoble urban environment by remote sensing

`Aneline DOLET and Mauro DALLA MURA`
**aneline.dolet@grenoble-inp.fr ; mauro.dalla-mura@grenoble-inp.fr**

## Introduction

Urban classification is a delineation of geographic areas, identifying individual urban
areas, rural areas and water areas of the nation. Discovering new phenomena and looking at
the evolution of the Earth’s scenery became the main issues of remote sensing so as to
provide data to experts. It can for instance help analysts to check the evolution of population
density, or even quantify deforestation. Studying in Grenoble leads us to work on images of
Grenoble so as to get accustomed to the main teledetection tools and to obtain a classification
of the different parts of the landscapes. First of all, the aim was to fetch images from
Sentinel2, a european satellite. Secondly, we had to find a method to have an automated
process which allows us to download Sentinel2 images. Then we expected to correctly
classify them in several items thanks to different methods like Machine Learning or Principal
Component Analysis (PCA). Finally we will apply our method to similar geographic areas in
order to check the proper functioning of our program. Our goal is to get a map of Grenoble
and its surroundings with a specific colour for each class.

## Project development

First of all, we had to skim through websites focused on getting satellite’s images of the
landscape of Grenoble and download those that are usable for our classification as there are
many settings that we could choose on the websites such as cloud coverage, the time bracket
where we want to crop the images and also which band we want to exploit.

After having recovered multispectral images containing twelve bands including RGB,
we had to read into the data. In order to interpret those images, we have been put on the path
to the PCA method.

We also used edge detection techniques to see their level of significance in dividing the
areas. We tried with the Sobel operator which is simple yet with high enough accuracy.
The operator uses two 3×3 kernels which are convolved with the original image to calculate
approximations of the derivatives – one for horizontal changes, and one for vertical.



### Resources

It is up to you to download the data you want. Be careful, before downloading a large number of images, check their format and be sure you know how to open/process them. As a reminder, we want to study an area around Grenoble and for images between the beginning of 2019 and today.
You will find the SENTINEL-2 data to be used at the following link: <https://www.copernicus.eu/en/access-data>.

If you want to pick some images manually top process first tests, follow this link: <https://apps.sentinel-hub.com/eo-browser/>
You can recover multispectral images from EO browser as JPG, PNG or TIFF.

## Here are some ideas...


It would be interesting to have an automated program to fetch Sentinel 2 data to avoid some cumbersome and time consuming moves on having to manually skim throught this humongus database.

A different field of analysis with the help of a machine learning algorithm is also a good way to carry on the project to allow us to view the accuracy of several classification methods and to weight their pros and cons.
