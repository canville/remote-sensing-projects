# -*- coding: utf-8 -*-
"""
Created on Thu May  7 09:11:54 2020

@author: pierr
"""
# This script allows the user to explore a hdf file.

# =============================================================================
# Packages
# =============================================================================

from pyhdf.SD import SD, SDC
import numpy as np
import pprint
import matplotlib.pyplot as plt

# =============================================================================
# Functions
# =============================================================================


def read_file(file, hdf_name):
    """
    Read the quantity of SDS files in the HDF file
    """
    print("\n")
    print("________________________________________")
    print("Access to ", hdf_name, " file : ")
    print("\n")
    result = np.asarray(file.info())
    print(file.info())
    print("The file contains ", result[0], " Scientific Datasets (SDS)")


def display_SDS_files(file, hdf_name):
    """
    Print all SDS files of the HDF file
    """
    print("\n")
    print("________________________________________")
    print("Access to the SDS of ", hdf_name, " file : ")
    print("\n")

    datasets_dic = file.datasets()

    for idx, sds in enumerate(datasets_dic.keys()):
        print(idx, sds)


def get_sds_obj(SDS_name, file, hdf_name):
    """
    Since we know the exact name
    of all SDS, we can get the data
    stored in one SDS easily
    """
    sds_obj = file.select(SDS_name)  # select sds
    data = sds_obj.get()  # get sds data
    print("\n")
    print("________________________________________")
    print("Access to datas from", SDS_name, "in the ", hdf_name, " file : ")
    print("\n")
    return sds_obj


def get_datas(SDS_name, file, hdf_name):
    """
    we can get the datas contained in an SDS
    """
    print("\n")
    print("________________________________________")
    print("Get datas from", SDS_name, "in the ", hdf_name, " file")
    print("\n")
    sds_obj = file.select(SDS_name)  # select sds
    data = sds_obj.get()  # get sds data
    print("Taille de la matrice de pixels = ", np.shape(data))
    return data


def get_size(data, SDS_name, hdf_name):
    print("\n")
    print("________________________________________")
    print(
        "Get size of datas from",
        SDS_name,
        "in the ",
        hdf_name,
        " file in a new window ",
    )
    print("\n")
    print("Taille de la matrice de pixels = ", np.shape(data))
    return np.shape(data)


def plt_datas(SDS_name, file, hdf_name):
    """
    we can plot the resulting matrix in a picture
    """
    print("\n")
    print("________________________________________")
    print(
        "Display of datas from",
        SDS_name,
        "in the ",
        hdf_name,
        " file in a new window ",
    )
    print("\n")
    sds_obj = file.select(SDS_name)  # select sds
    data = sds_obj.get()  # get sds data
    print("Taille de la matrice de pixels = ", np.shape(data))
    plt.imshow(data)
    plt.title(SDS_name)
    plt.show()


def display_datas_attributes(SDS_name, sds_obj, hdf_name):
    """
    it is necessary to transform the data
    by using the information stored in the
    sds attributes. To read the attributes :
    """
    print("\n")
    print("________________________________________")
    print(
        "Display of attributes from", SDS_name, "in the ", hdf_name, " file :"
    )
    print("\n")
    pprint.pprint(sds_obj.attributes())


def get_datas_attributes(SDS_name, sds_obj, hdf_name):
    """
    it is necessary to transform the data
    by using the information stored in the
    sds attributes. To read the attributes :
    """
    print("\n")
    print("________________________________________")
    print("Get attributes from", SDS_name, "in the ", hdf_name, " file : ")
    print("\n")
    return sds_obj.attributes()


def display_datas(SDS_name, hdf, hdf_name):
    sds_obj = hdf.select(SDS_name)
    datas = sds_obj.get()
    print("\n")
    print("________________________________________")
    print("\nDatas from ", hdf_name, "in the SDS : ", SDS_name, " : ", datas)
    print("\n")


# =============================================================================
# =============================================================================
# Main programm
# =============================================================================
# =============================================================================

hdf_name = "MOD10A1.A2019003.h18v04.006.2019005025145.hdf"

# =============================================================================
# Read the quantity of files
# =============================================================================

hdf = SD(hdf_name, SDC.READ)
read_file(hdf, hdf_name)

# =============================================================================
# Print all SDS files
# =============================================================================

display_SDS_files(hdf, hdf_name)
# =============================================================================
# Print the data of a specific SDS
# =============================================================================

display_datas("Day_CMG_Snow_Cover", hdf, hdf_name)
# =============================================================================
# Print the attributes of a specific SDS
# =============================================================================

display_datas_attributes(
    "Day_CMG_Snow_Cover",
    get_sds_obj("Day_CMG_Snow_Cover", hdf, hdf_name),
    hdf_name,
)
