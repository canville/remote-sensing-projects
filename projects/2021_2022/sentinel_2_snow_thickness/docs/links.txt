EO Browser:
https://apps.sentinel-hub.com/eo-browser

Pixel value detection:
https://fr.inettools.net/image/d%C3%A9finir-la-couleur-de-pixel-d-une-image-en-ligne

Method used for the estimation of snow depth:
https://www.shf-lhb.org/articles/lhb/pdf/1978/05/lhb1978042.pdf

Article used to estimate the precision of the snow depth estimation method:
https://www.placegrenet.fr/2021/11/29/isere-chutes-de-neige-abondantes-et-perturbations-le-departement-maintenu-en-vigilance-orange/552667 